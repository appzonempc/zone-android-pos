package com.appzonegroup.zonepos.helpers

import com.appzonegroup.zonepos.db.entities.TransactionEntity

abstract class ListItem {
    companion object {
        val TYPE_DATE = 0
        val TYPE_GENERAL = 1
    }

    abstract fun getType():Int
}

 class GeneralItem(var orderDetailModel: TransactionEntity): ListItem() {

     override fun getType(): Int{
       return TYPE_GENERAL
     }

 }

class DateItem(var date: String): ListItem() {

    override fun getType(): Int {
        return TYPE_DATE
    }

}

class MerchantItem(var merchantOrderDetails: ArrayList<ArrayList<TransactionEntity>>?):ListItem() {
    override fun getType(): Int {

        return TYPE_GENERAL
    }

}