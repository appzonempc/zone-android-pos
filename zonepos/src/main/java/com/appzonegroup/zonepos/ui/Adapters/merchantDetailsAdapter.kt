package com.appzonegroup.zonepos.ui.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.appzonegroup.zonepos.R
import com.appzonegroup.zonepos.helpers.DateItem
import com.appzonegroup.zonepos.helpers.GeneralItem
import com.appzonegroup.zonepos.helpers.ListItem
import com.appzonegroup.zonepos.ui.utils.toNumberFormat

class MerchantDetailsAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    var consolidatedList: ArrayList<ListItem> = ArrayList()

    fun setItems(list:ArrayList<ListItem>){
        consolidatedList.clear()
        consolidatedList.addAll(list)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        lateinit var viewHolder: RecyclerView.ViewHolder

        val inflater = LayoutInflater.from(parent.context)
        when(viewType){
            ListItem.TYPE_GENERAL ->{
                val v1 = inflater.inflate(R.layout.pending_order_item, parent, false)

                viewHolder = MerchantItemViewHolder(v1)
            }
            ListItem.TYPE_DATE -> {
                val v2 = inflater.inflate(R.layout.date_item, parent, false)
                viewHolder = DateItemViewHolder(v2)
            }
        }

        return viewHolder
    }

    override fun getItemCount() = consolidatedList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder.itemViewType){
            ListItem.TYPE_DATE -> {
                val dateItem = consolidatedList.get(position) as DateItem
                val dateViewHolder = holder as DateItemViewHolder

                dateViewHolder.itemView.findViewById<TextView>(R.id.order_date).text = dateItem.date
            }
            ListItem.TYPE_GENERAL -> {
                val generalItem = consolidatedList.get(position) as GeneralItem
                val generalItemViewHolder = holder as MerchantItemViewHolder

                generalItemViewHolder.itemView.apply {
                    findViewById<TextView>(R.id.transactionid).text =
                        "#${generalItem.orderDetailModel.transactiondate}"
                    findViewById<TextView>(R.id.price).text =
                        generalItem.orderDetailModel.amount.toString().toNumberFormat(true)


                    findViewById<TextView>(R.id.status_view).text = generalItem.orderDetailModel.status
                    when (generalItem.orderDetailModel.status) {
                        "COMPLETED" -> {
                            findViewById<TextView>(R.id.status_view).setTextColor(resources.getColor(R.color.green))
                        }
                        "PENDING" -> {
                            findViewById<TextView>(R.id.status_view).setTextColor(resources.getColor(android.R.color.holo_orange_light))
                        }
                        "ERROR" -> {
                            findViewById<TextView>(R.id.status_view).setTextColor(resources.getColor(android.R.color.holo_red_light))
                        }
                    }
                }


            }
        }
    }

    override fun getItemViewType(position: Int) = consolidatedList[position].getType()

    inner class MerchantItemViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
    inner class DateItemViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
}