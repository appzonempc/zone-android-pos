package com.appzonegroup.zonepos.ui.Adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class HomePageAdapter(var context: Context?, var fragmentList: List<Fragment>, var fragmentTitle: List<String>, var fragmentManager: FragmentManager): FragmentStatePagerAdapter(fragmentManager){

    override fun getItem(position: Int) = fragmentList.get(position)

    override fun getCount() = fragmentList.size

    override fun getPageTitle(position: Int) = fragmentTitle.get(position)
}