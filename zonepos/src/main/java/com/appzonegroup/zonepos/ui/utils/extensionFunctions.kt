package com.appzonegroup.zonepos.ui.utils

import android.app.Activity
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.SimpleDateFormat
import java.util.*


//Extension functions
fun String.toNumberFormat(includeCurrency: Boolean): String{
    val symbols = DecimalFormatSymbols()
    symbols.decimalSeparator = ','
    val decimalFormat = if(includeCurrency) { DecimalFormat("₦ ###,###,###,###", symbols)} else{ DecimalFormat("###,###,###,###", symbols)}

    val formattedString = decimalFormat.format(this.toDouble())
    return formattedString
}

fun Long.millisecondsToDateFormat(format:String):String {
    val formatter = SimpleDateFormat(format)
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this
    return formatter.format(calendar.time)
}

inline fun <reified T: Any> Activity.extra(key: String, default: T? = null) = lazy {
    val value = intent?.extras?.get(key)
    if (value is T) value else default
}
inline fun <reified T: Any> Activity.extraNotNull(key: String, default: T? = null) = lazy {
    val value = intent?.extras?.get(key)
    requireNotNull(if (value is T) value else default) { key }
}

