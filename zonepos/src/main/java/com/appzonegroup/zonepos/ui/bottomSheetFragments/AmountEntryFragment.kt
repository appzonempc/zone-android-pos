package com.appzonegroup.zonepos.ui.bottomSheetFragments


import android.os.Bundle
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.appzonegroup.zonepos.R
import com.appzonegroup.zonepos.ui.bottomSheetFragments.accountEntryFragment.AccountPinEntry

class AmountEntryFragment() : BottomSheetFragmentRoot(), View.OnClickListener {
    private val keyValues = SparseArray<String>()
    private lateinit var amountEntryView: View
    private lateinit var editText: AppCompatEditText


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.amount_entry, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        amountEntryView = view
        editText = amountEntryView.findViewById(R.id.input_text)

        initAmountEntryBottomSheetViews()
    }


    private fun initAmountEntryBottomSheetViews() {

        with(amountEntryView) {
            findViewById<AppCompatTextView>(R.id.one).setOnClickListener(this@AmountEntryFragment)
            findViewById<AppCompatTextView>(R.id.two).setOnClickListener(this@AmountEntryFragment)
            findViewById<AppCompatTextView>(R.id.three).setOnClickListener(this@AmountEntryFragment)
            findViewById<AppCompatTextView>(R.id.four).setOnClickListener(this@AmountEntryFragment)
            findViewById<AppCompatTextView>(R.id.five).setOnClickListener(this@AmountEntryFragment)
            findViewById<AppCompatTextView>(R.id.six).setOnClickListener(this@AmountEntryFragment)
            findViewById<AppCompatTextView>(R.id.seven).setOnClickListener(this@AmountEntryFragment)
            findViewById<AppCompatTextView>(R.id.eight).setOnClickListener(this@AmountEntryFragment)
            findViewById<AppCompatTextView>(R.id.nine).setOnClickListener(this@AmountEntryFragment)
            findViewById<AppCompatTextView>(R.id.zero).setOnClickListener(this@AmountEntryFragment)
            findViewById<AppCompatImageView>(R.id.backspace).setOnClickListener(this@AmountEntryFragment)
            findViewById<AppCompatImageView>(R.id.launch_pin).setOnClickListener(this@AmountEntryFragment)
        }

        keyValues.put(R.id.one, "1")
        keyValues.put(R.id.two, "2")
        keyValues.put(R.id.three, "3")
        keyValues.put(R.id.four, "4")
        keyValues.put(R.id.five, "5")
        keyValues.put(R.id.six, "6")
        keyValues.put(R.id.seven, "7")
        keyValues.put(R.id.eight, "8")
        keyValues.put(R.id.nine, "9")
        keyValues.put(R.id.zero, "0")
    }


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.backspace -> {
                editText.text?.clear()
            }
            R.id.launch_pin -> {

                val bundle = Bundle()
                bundle.putString("amount", editText.text.toString())

                val pinEntry = AccountPinEntry()
                pinEntry.arguments = bundle
                pinEntry.show(activity!!.supportFragmentManager , pinEntry.tag)
                dismiss()
            }
            else -> {

                val id = v?.id
                val value = id?.let { keyValues.get(it) }
                amountEntryView.findViewById<AppCompatEditText>(R.id.input_text).text?.append(value)
            }
        }
    }


    companion object {
        //static method for debugging
        private val TAG  = AmountEntryFragment::class.java.simpleName
    }
}
