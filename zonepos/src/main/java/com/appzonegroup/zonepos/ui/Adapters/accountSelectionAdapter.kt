package com.appzonegroup.zonepos.ui.Adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class AccountSelectionAdapter(var fragmentManager: FragmentManager, var fragmentList: List<Fragment> ): FragmentStatePagerAdapter(fragmentManager){
    override fun getItem(position: Int) = fragmentList.get(position)

    override fun getCount()=  fragmentList.size
}