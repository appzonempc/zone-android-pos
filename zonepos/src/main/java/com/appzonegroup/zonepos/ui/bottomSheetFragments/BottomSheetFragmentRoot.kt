package com.appzonegroup.zonepos.ui.bottomSheetFragments


import android.app.Application
import android.app.Dialog
import android.os.Bundle
import android.view.View
import com.appzonegroup.zonepos.activities.ZoneActivity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment


open class BottomSheetFragmentRoot: BottomSheetDialogFragment() {


    protected fun getAppinstance(): Application {
        val homeActivity = activity as ZoneActivity
        return homeActivity.application
    }

    //Make the dialog fullscreen
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bottomSheetDialog =  super.onCreateDialog(savedInstanceState) as BottomSheetDialog
        bottomSheetDialog.setOnShowListener {
            val dialog = it as BottomSheetDialog
            val bottomSheet = dialog.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
            BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED)
            BottomSheetBehavior.from(bottomSheet).setSkipCollapsed(true)
            BottomSheetBehavior.from(bottomSheet).setHideable(true)
        }

        return bottomSheetDialog
    }

}
