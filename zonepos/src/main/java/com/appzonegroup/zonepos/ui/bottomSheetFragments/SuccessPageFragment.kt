package com.appzonegroup.zonepos.ui.bottomSheetFragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

import android.graphics.Paint
import com.appzonegroup.zonepos.ui.utils.millisecondsToDateFormat


class SuccessPageFragment : BottomSheetFragmentRoot() {

    private lateinit var mView: View
    private lateinit var oldAmountView: TextView
    private lateinit var newAmountView: TextView
    private lateinit var accountNumberView: TextView
    private lateinit var dateTimeView: TextView
    private lateinit var loyaltyView: TextView
    private var oldAmount:String? =null
    private var newAmount:String? =null
    private var date:Long? = null
    private var accountNumber:String? = null
    private var loyalty: Double? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        oldAmount = arguments?.getString("oldAmount")
        newAmount = arguments?.getString("newAmount")
        date = arguments?.getLong("date")
        accountNumber = arguments?.getString("accountNumber")
        loyalty = arguments?.getDouble("loyalty")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = inflater.inflate(
        com.appzonegroup.zonepos.R.layout.fragment_success_page, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mView = view
        oldAmountView = mView.findViewById(com.appzonegroup.zonepos.R.id.oldAmount)
        newAmountView = mView.findViewById(com.appzonegroup.zonepos.R.id.newAmount)
        accountNumberView = mView.findViewById(com.appzonegroup.zonepos.R.id.account_number)
        dateTimeView = mView.findViewById(com.appzonegroup.zonepos.R.id.date_time)
        loyaltyView = mView.findViewById(com.appzonegroup.zonepos.R.id.loyalty)

        oldAmountView.paintFlags = oldAmountView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        oldAmountView.text = oldAmount
        newAmountView.text = newAmount
        accountNumberView.text = accountNumber
        dateTimeView.text = date?.millisecondsToDateFormat("EEE, MMM d,yyyy hh:mm:ss")
        loyaltyView.text = loyalty?.toString()


            mView.findViewById<Button>(com.appzonegroup.zonepos.R.id.close_btn).setOnClickListener {

                dismissAllowingStateLoss()

            }
    }



}
