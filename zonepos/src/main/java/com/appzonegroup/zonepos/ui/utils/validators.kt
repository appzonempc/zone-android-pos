package com.appzonegroup.zonepos.ui.utils

import androidx.annotation.Nullable

object Validator {


    fun <T> verifiyNotNull(@Nullable data:T, message:String):T{
        if (data == null) {
            throw NullPointerException(message)
        }
        return data
    }
}