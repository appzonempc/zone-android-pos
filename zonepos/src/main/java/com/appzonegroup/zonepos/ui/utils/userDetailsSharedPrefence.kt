package com.appzonegroup.zonepos.ui.utils

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences


class UserDetailsSharedPreferences(context: Context){
    val TOKEN = "token"
    val IS_INITIALIZED = "is_initialized"

    var sharedPreferences: SharedPreferences = context.getSharedPreferences("user_details", MODE_PRIVATE)
    var editor = sharedPreferences.edit()


    fun setUserToken(token:String?){
       token?.let {
           editor.putString(TOKEN, it)
           editor.commit()
       }
    }

    fun getUserToken() = sharedPreferences.getString(TOKEN, "token")

    fun setInitializedStatus(data: Boolean) {
        editor.putBoolean(IS_INITIALIZED, data)
        editor.commit()
    }

    fun getInitializedStatus() = sharedPreferences.getBoolean(IS_INITIALIZED, false)
}