package com.appzonegroup.zonepos.ui.utils

import android.util.Log
import com.appzonegroup.zonepos.db.entities.TransactionEntity
import com.appzonegroup.zonepos.helpers.DateItem
import com.appzonegroup.zonepos.helpers.GeneralItem
import com.appzonegroup.zonepos.helpers.ListItem
import com.appzonegroup.zonepos.helpers.MerchantItem

class ListArrangement(){
    fun groupDataIntoHashMap(orderDetails: List<TransactionEntity>?): HashMap<String, MutableList<TransactionEntity>> {

        //date ->list of transactions
        val groupedHashMap = HashMap<String, MutableList<TransactionEntity>>()

        if (orderDetails != null) {
            for(orderDetail in orderDetails){
                val hashMapKey = orderDetail.transactiondate.millisecondsToDateFormat("EEE, MMM d,yyyy")

                if(groupedHashMap.containsKey(hashMapKey)){
                    // The key is already in the HashMap; add the pojo object
                    // against the existing key.
                    groupedHashMap[hashMapKey]?.add(orderDetail)
                } else {
                    // The key is not there in the HashMap; create a new key-value pair
                    val list = ArrayList<TransactionEntity>()
                    list.add(orderDetail)
                    groupedHashMap[hashMapKey] = list
                }
            }
        }

        return groupedHashMap
    }



    fun createConsolidatedListForSingleItem(orderDetails: List<TransactionEntity>?): ArrayList<ListItem>{

        val groupedHashmap = groupDataIntoHashMap(orderDetails)

        //list with both date and items arranged serially
        var consolidatedList = ArrayList<ListItem>()

        for(date in groupedHashmap.keys) {
            val dateitem = DateItem(date)
            consolidatedList.add(dateitem)

            groupedHashmap[date]?.forEach {
                val generalItem = GeneralItem(it)
                consolidatedList.add(generalItem)
            }
        }

        return consolidatedList
    }

    //group data based on date
    fun groupMerchantDataIntoHasHMap(merchants: ArrayList<ArrayList<TransactionEntity>>?): HashMap<String, ArrayList<ArrayList<TransactionEntity>>>{
        val groupedHashMap = HashMap<String, ArrayList<ArrayList<TransactionEntity>>>()
        //date -> list of distinct merchants -> (list of transactions per merchant)

        Log.d("Merchant details", "${merchants?.size}")
        if(merchants !=null) {
            for((position,merchantdetails) in merchants.withIndex()){
                val dummyList = ArrayList<ArrayList<TransactionEntity>>()
                for(distinctDetail in merchantdetails) {

                    val hashMapKey = distinctDetail.transactiondate.millisecondsToDateFormat("EEE, MMM d,yyyy")
                    Log.d("Key", hashMapKey)
                    if(groupedHashMap.containsKey(hashMapKey)) {

                        groupedHashMap[hashMapKey]?.get(position)?.add(distinctDetail)
                    } else {
                        val list = ArrayList<TransactionEntity>()
                        list.add(distinctDetail)

                        dummyList.add(list)
                        groupedHashMap[hashMapKey] = dummyList
                    }
                }
                Log.d("Incremented Size", "${groupedHashMap.keys.size}")
            }
        }

        return groupedHashMap
    }


    fun createConsolidatedListForMerchant(merchantDetails: ArrayList<ArrayList<TransactionEntity>>?): ArrayList<ListItem>{

        val groupedDateAndMerchantHashmap = groupMerchantDataIntoHasHMap(merchantDetails)
        //list with both date and items arranged serially
        var consolidatedList = ArrayList<ListItem>()
        Log.d("item length", "${groupedDateAndMerchantHashmap.keys.size}")

        for(date in groupedDateAndMerchantHashmap.keys) {
            //later on remove this, for the history adapter
            val dateitem = DateItem(date)
            consolidatedList.add(dateitem)

            var list = groupedDateAndMerchantHashmap[date]
//            var list = groupedDateAndMerchantHashmap[date]?.get(0) just pass the list of items

            val merchantItem = MerchantItem(list)
            consolidatedList.add(merchantItem)

        }

        return consolidatedList
    }


}