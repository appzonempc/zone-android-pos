package com.appzonegroup.zonepos.ui.bottomSheetFragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

import com.appzonegroup.zonepos.R


class ExpiredQrFragment : BottomSheetFragmentRoot() {
    private lateinit var mView: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = inflater.inflate(R.layout.fragment_expired_qr, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mView = view

        mView.findViewById<Button>(R.id.close_btn).setOnClickListener { dismiss() }
    }

}
