package com.appzonegroup.zonepos.ui.bottomSheetFragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.appzonegroup.zonepos.R
import com.appzonegroup.zonepos.db.AppDatabase
import com.appzonegroup.zonepos.db.entities.TransactionEntity
import com.appzonegroup.zonepos.ui.utils.UserDetailsSharedPreferences
import com.appzonegroup.zonepos.ui.utils.toNumberFormat
import com.appzonegroup.zonepos.viewmodels.SingleTransactionViewModel
import com.appzonegroup.zonepos.viewmodels.TransactionViewModelFactory
import java.util.concurrent.Executors
import kotlin.collections.HashMap


class QrFragment : BottomSheetFragmentRoot() {
    private lateinit var qrView: View
    private var amountText: String? = null
    private var accountName: String? = null
    private var accountNumber: String? = null
    private var accountType: String? = null
    private var currencyCode: String? = null
    private var bankName: String? = null
    private var phoneNumber: String? = null
    private var fullname: String? = null

    private val mExecutor = Executors.newFixedThreadPool(5)
    private lateinit var mDb:AppDatabase
    private lateinit var factory: TransactionViewModelFactory
    private lateinit var transactionViewModel: SingleTransactionViewModel
    private lateinit var userDetailsSharedPreferences: UserDetailsSharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDb = AppDatabase.getInstance(getAppinstance())
        userDetailsSharedPreferences = UserDetailsSharedPreferences(getAppinstance())

        amountText = arguments?.getString("Amount")
        accountName = arguments?.getString("AccountName")
        accountNumber = arguments?.getString("AccountNumber")
        accountType = arguments?.getString("AccountType")
        currencyCode = arguments?.getString("CurrencyCode")
        bankName = arguments?.getString("BankName")
        phoneNumber = arguments?.getString("PhoneNumber")
        fullname = arguments?.getString("FullName")

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)= inflater.inflate(
        R.layout.fragment_qr, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        qrView = view
        qrView.findViewById<TextView>(R.id.amount).text = amountText?.toNumberFormat(true)

        amountText?.let {
            initQr(it)
        }

    }


    private fun initQr(amount:String) {

        qrView.findViewById<Button>(R.id.payment_status_btn).setOnClickListener {

        }

        //handle encoding data in the map
        val discount = amount.toDouble() * 0.05
        var fcmToken = userDetailsSharedPreferences.getUserToken()
        var timeInMillis = System.currentTimeMillis()

        Log.d("QRFRAGMENT", "Currrent time in millis is ${timeInMillis}")


        var map = HashMap<String, Any>().apply {
            phoneNumber?.let { put("PhoneNumber", it) }
            accountNumber?.let { put("AccountNumber", it) }
            bankName?.let { put("Bank", it) }
            put("DateTime", timeInMillis.toString())
            fullname?.let { put("FullName", it) }
            put("Loyalty", discount.toString())
            put("amount", amount)
            if (fcmToken != null) {
                put("token", fcmToken)
            }
        }


        var qrdata = Gson().toJson(map)
        Log.d(TAG, qrdata)
        val multiFormatWriter = MultiFormatWriter()
        try {
            val bitMatrix = multiFormatWriter.encode(qrdata, BarcodeFormat.QR_CODE,300,300)
            val barcodeEncoder = BarcodeEncoder()
            val bitmap = barcodeEncoder.createBitmap(bitMatrix)
            qrView.findViewById<ImageView>(R.id.qrCodeImageView).setImageBitmap(bitmap)

            insertIntoDb(amount, qrdata, discount, timeInMillis)

        } catch (e: WriterException) {
            e.printStackTrace()
        }
    }


    private fun insertIntoDb(amount: String, qrData: String, discount: Double, timeInMillis:Long) {

        val newAmount = amount.toDouble() - discount
        mExecutor.execute {

            val transactionEntity = TransactionEntity(username = "", status = "pending", transactiondate = timeInMillis, amount = amount.toDouble(), qrdata = qrData, loyalty = discount)

            val id = mDb.transactionDao().insertTransaction(transactionEntity)

            activity?.runOnUiThread {
                Log.d(TAG, "Start listening for changes -> $id")
                factory = TransactionViewModelFactory(mDb, id)
                transactionViewModel = ViewModelProviders.of(this, factory).get(SingleTransactionViewModel::class.java)
                transactionViewModel.getUpdateTransaction().observe(this, object:Observer<TransactionEntity> {
                    override fun onChanged(t: TransactionEntity?) {
                        //todo: if data received from api is failure or expired, handle appropriate call
                        if(t?.status != "pending") {
                            Log.d(TAG, "item updated from pending to completed")
                            launchSuccessPage(amount, newAmount, timeInMillis, discount, "2084827124")
                            transactionViewModel.getUpdateTransaction().removeObserver(this)
                            dismiss()
                        }else{
                            Log.d(TAG, "item still in pending")
                        }
                    }

                })


                Log.d(TAG, "Item inserted into the db")
            }
        }
    }


    fun launchSuccessPage(amount:String, newAmount: Double, timeInMillis:Long, loyalty:Double, accountNumber:String){
        val bundle = Bundle()
        bundle.putString("oldAmount", amount.toNumberFormat(true))
        bundle.putString("newAmount", newAmount.toString().toNumberFormat(true))
        bundle.putLong("date", timeInMillis)
        bundle.putString("accountNumber", accountNumber)
        bundle.putDouble("loyalty", loyalty)

        val successPageFragment = SuccessPageFragment()
        successPageFragment.arguments = bundle
        successPageFragment.show(activity!!.supportFragmentManager , successPageFragment.tag)

        dismiss()
    }

    fun launchFailurePage(){
        val failureFragment = FailurePageFragment()
        failureFragment.show(activity!!.supportFragmentManager , failureFragment.tag)
        dismiss()
    }

    fun launchExpiredPage() {
        val expiredQrFragment = ExpiredQrFragment()
        expiredQrFragment.show(activity!!.supportFragmentManager , expiredQrFragment.tag)
        dismiss()
    }
    companion object {
        private var TAG = QrFragment.javaClass.simpleName
    }


}
