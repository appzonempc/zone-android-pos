package com.appzonegroup.zonepos.ui


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.appzonegroup.zonepos.db.AppDatabase
import com.appzonegroup.zonepos.db.entities.TransactionEntity
import com.appzonegroup.zonepos.ui.Adapters.HistoryAdapter
import com.appzonegroup.zonepos.ui.utils.ListArrangement
import com.appzonegroup.zonepos.viewmodels.SuccessAndFailedTransactionViewModel
import com.appzonegroup.zonepos.viewmodels.SuccessAndFailedTransactionsFactory
import com.appzonegroup.zonepos.viewmodels.TransactionTypeViewModel
import com.appzonegroup.zonepos.viewmodels.TransactionsTypeFactory
import android.graphics.drawable.InsetDrawable
import android.widget.RelativeLayout
import com.appzonegroup.zonepos.activities.PendingDetailActivity
import com.appzonegroup.zonepos.activities.ZoneActivity
import com.google.android.material.bottomsheet.BottomSheetBehavior


class HistoryFragment : Fragment() {
    private lateinit var mView: View
    private lateinit var pendingOrdersView: ConstraintLayout
    private lateinit var merchantRecyclerView: RecyclerView
    private lateinit var loyaltyBottomSheet: RelativeLayout
    private lateinit var mDb: AppDatabase
    private lateinit var pendingViewModel: TransactionTypeViewModel
    private lateinit var successAndFailedViewModel: SuccessAndFailedTransactionViewModel
    private lateinit var pendingFactory: TransactionsTypeFactory
    private lateinit var successAndFailedTransactionFactory: SuccessAndFailedTransactionsFactory
    private lateinit var homeActivity: ZoneActivity
    private  var historyAdapter: HistoryAdapter?=null
    val listArr =  ListArrangement()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        homeActivity = activity as ZoneActivity
        mDb = AppDatabase.getInstance(homeActivity.applicationContext)


        //pending view model init
        pendingFactory = TransactionsTypeFactory(mDb, "pending")
        pendingViewModel = ViewModelProviders.of(this, pendingFactory).get(TransactionTypeViewModel::class.java)

        //sucessful and failed transactions viewmodel init
        successAndFailedTransactionFactory = SuccessAndFailedTransactionsFactory(mDb)
        successAndFailedViewModel = ViewModelProviders.of(this, successAndFailedTransactionFactory).get(SuccessAndFailedTransactionViewModel::class.java)

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = inflater.inflate(
        com.appzonegroup.zonepos.R.layout.fragment_history, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mView = view
        pendingOrdersView = mView.findViewById(com.appzonegroup.zonepos.R.id.pending_orders)
        merchantRecyclerView = mView.findViewById(com.appzonegroup.zonepos.R.id.merchant_recycler_view)
        loyaltyBottomSheet = mView.findViewById(com.appzonegroup.zonepos.R.id.loyalty_bottomsheet)
        setUpRecyclerView()
        setUpBottomSheet()

        //listen to pending data
        listenToPendingTransactions()

        listenToSuccessfulAndFailedTransactions()
    }

    fun setUpBottomSheet(){
        var behavior = BottomSheetBehavior.from(loyaltyBottomSheet)

        behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    fun listenToPendingTransactions() {
        pendingViewModel.getTransactionsList().observe(this, Observer {
            Log.d("HistoryFragment", "Pending Item updated")
            if(it.isNotEmpty()) {
                pendingOrdersView.visibility = View.VISIBLE
                mView.findViewById<TextView>(com.appzonegroup.zonepos.R.id.pending_count).text = it.size.toString()
                pendingOrdersView.setOnClickListener {
                    val intent = Intent(activity, PendingDetailActivity::class.java)
                    startActivity(intent)

                }
            }else {
                pendingOrdersView.visibility = View.GONE
            }
        })
    }

    fun listenToSuccessfulAndFailedTransactions(){

        //listen to successful and failed transaction
        successAndFailedViewModel.getSuccessAndFailedTransaction().observe(this, Observer {
            Log.d("HistoryFragment", "Merchant Item updated")
            if(it.isNotEmpty()){

                //todo:transfer this piece of code below to view model which handles data in the background, and just returns data to be passed to the adapter-simple, your activity should only display ui
                //this list holds distinct merchants(each merchant holds a list of all their transactions)
                val listOfDistinctMerchants = ArrayList<ArrayList<TransactionEntity>>()

                //arrange merchants username -> distinct merchantList
                val groupedMerchantHashMap = HashMap<String, MutableList<TransactionEntity>>()



                for(merchantDetail in it){
                    val hashMapKey = merchantDetail.username

                    if(groupedMerchantHashMap.containsKey(hashMapKey)) {

                        groupedMerchantHashMap[hashMapKey]?.add(merchantDetail)
                    } else {

                        val list = ArrayList<TransactionEntity>()
                        list.add(merchantDetail)
                        groupedMerchantHashMap[hashMapKey] = list
                    }
                }

                //transform data in to list of distinct merchants(each containing a list of transactions)
                for((position, key) in groupedMerchantHashMap.keys.withIndex()){
                    Log.d("History", "$position -> $key")
//
                    val dummyList = ArrayList<TransactionEntity>()
                    groupedMerchantHashMap[key]?.withIndex()?.forEach { (pos, item) ->
                        dummyList.add(item)
                    }
                    listOfDistinctMerchants.add(dummyList)
                }

                Log.d("Merchant list size", ">>>>>>>>>>>>${listOfDistinctMerchants.size}")
//
                historyAdapter?.setItems(listOfDistinctMerchants)
            }
        })
    }

    private fun setUpRecyclerView() {
        historyAdapter = HistoryAdapter(activity?.applicationContext)
        merchantRecyclerView.adapter = historyAdapter

        val lm = LinearLayoutManager(activity)
        lm.orientation = RecyclerView.VERTICAL
        merchantRecyclerView.layoutManager = lm

        val ATTRS = intArrayOf(android.R.attr.listDivider)

        val a = context?.obtainStyledAttributes(ATTRS)
        val divider = a?.getDrawable(0)
        val insetLeft = resources.getDimensionPixelSize(com.appzonegroup.zonepos.R.dimen.recycler_margin_left)
        val insetRight = resources.getDimensionPixelSize(com.appzonegroup.zonepos.R.dimen.recycler_margin_right)
        val insetDivider = InsetDrawable(divider, insetLeft, 0, insetRight, 0)
        a?.recycle()

        val dividerItemDecoration = DividerItemDecoration(merchantRecyclerView.context, lm.orientation)
        dividerItemDecoration.setDrawable(insetDivider)
        merchantRecyclerView.addItemDecoration(dividerItemDecoration)

    }



}
