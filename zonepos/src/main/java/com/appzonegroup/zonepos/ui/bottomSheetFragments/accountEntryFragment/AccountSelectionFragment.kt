package com.appzonegroup.zonepos.ui.bottomSheetFragments.accountEntryFragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.appzonegroup.zonepos.R
import com.appzonegroup.zonepos.models.Account


class AccountSelectionFragment : Fragment() {

    var mAccount: Account? = null
    var accountNameView: TextView? = null
    var accountNumberView: TextView? = null
    var accountTypeView: TextView? = null


    companion object {
        fun getInstance(account: Account): AccountSelectionFragment {
            var accountSelectionFragment =
                AccountSelectionFragment()

            var bundle = Bundle()
            bundle.putParcelable("account", account)
            accountSelectionFragment.arguments = bundle

            return accountSelectionFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mAccount = arguments?.getParcelable("account")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =inflater.inflate(R.layout.fragment_account_selection, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        accountNameView = view.findViewById(R.id.accountName)
        accountNumberView = view.findViewById(R.id.accountNumber)
        accountTypeView = view.findViewById(R.id.accountType)

        init()
    }

    private fun init() {
        accountNameView?.text = mAccount?.accountName
        accountNumberView?.text = mAccount?.accountNumber
        accountTypeView?.text = mAccount?.accountType
    }

}
