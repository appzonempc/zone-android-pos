package com.appzonegroup.zonepos.ui.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.appzonegroup.zonepos.R
import com.appzonegroup.zonepos.activities.MerchantDetailActivity
import com.appzonegroup.zonepos.db.entities.TransactionEntity
import com.appzonegroup.zonepos.helpers.DateItem
import com.appzonegroup.zonepos.helpers.ListItem
import com.appzonegroup.zonepos.helpers.MerchantItem
import com.appzonegroup.zonepos.ui.utils.toNumberFormat

class HistoryAdapter(var context: Context?): RecyclerView.Adapter<HistoryAdapter.MerchantItemViewHolder>(){
    var merchantList: ArrayList<ArrayList<TransactionEntity>> = ArrayList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MerchantItemViewHolder {
        return  MerchantItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.merchant_item_view,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MerchantItemViewHolder, position: Int) {
       val merchant = merchantList.get(position)

        bindDataToUI(merchant,holder)
    }

    private fun bindDataToUI(merchant: ArrayList<TransactionEntity>, viewHolder: MerchantItemViewHolder) {
        var totalLoyaltyPoints = 0.0


        merchant.withIndex().forEach { (index, merchantdetails) ->
            totalLoyaltyPoints += merchantdetails.loyalty
        }

        //        merchantItemViewHolder.itemView.apply {
//            findViewById<TextView>(R.id.merch_name).text = merchantItem.merchantOrderDetails?.get(0)?.get(0)?.username
//            var LoyaltyPoints = "${totalLoyaltyPoints.toString().toNumberFormat(false)} points"
//            findViewById<TextView>(R.id.merch_loyalty_points).text = LoyaltyPoints
//            findViewById<TextView>(R.id.total_amount).text =
//                    merchantItem.merchantOrderDetails?.get(0)?.size?.minus(1)?.let {
//                        merchantItem.merchantOrderDetails?.get(0)?.get(it)?.amount.toString()
//                    }
//            findViewById<TextView>(R.id.status).text =
//                    merchantItem.merchantOrderDetails?.get(0)?.size?.minus(1)?.let {
//                        merchantItem.merchantOrderDetails?.get(0)?.get(it)?.status
//                    }
//            findViewById<TextView>(R.id.merch_initial).text = initials
//
//            if(findViewById<TextView>(R.id.status).text == "COMPLETED"){
//                findViewById<TextView>(R.id.status).setTextColor(resources.getColor(R.color.green))
//            }
//        }
//
//        merchantItemViewHolder.itemView.setOnClickListener {
//            val intent = Intent(context, MerchantDetailActivity::class.java)
//            intent.putParcelableArrayListExtra("merchantDetails", merchantItem.merchantOrderDetails?.get(0))
//            context?.startActivity(intent)
//        }

        var initials = merchant.get(0).username.substring(0,2)

        viewHolder.itemView.apply {
            //add username
            findViewById<TextView>(R.id.merch_name).text = merchant.get(0).username
            //add loyalty point
            var loyaltyPoints = "${totalLoyaltyPoints.toString().toNumberFormat(false)} points"
            findViewById<TextView>(R.id.merch_loyalty_points).text = loyaltyPoints

            //add last amount
            findViewById<TextView>(R.id.total_amount).text = merchant[merchant.size.minus(1)].amount.toString()

            //add last transaction status
            findViewById<TextView>(R.id.status).text = merchant[merchant.size.minus(1)].status
            //add initials or image
            findViewById<TextView>(R.id.merch_initial).text = initials

            if(findViewById<TextView>(R.id.status).text == "COMPLETED"){
                findViewById<TextView>(R.id.status).setTextColor(resources.getColor(R.color.green))
            }else{
                findViewById<TextView>(R.id.status).setTextColor(resources.getColor(android.R.color.holo_red_light))
            }

        }

        viewHolder.itemView.setOnClickListener {
            val intent = Intent(context, MerchantDetailActivity::class.java)
            intent.putParcelableArrayListExtra("merchantDetails", merchant)
            context?.startActivity(intent)
        }
    }


//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
//        lateinit var viewHolder: RecyclerView.ViewHolder
//
//        val inflater = LayoutInflater.from(parent.context)
//        when(viewType){
//            ListItem.TYPE_GENERAL -> {
//                val v1 = inflater.inflate(R.layout.merchant_item_view, parent, false)
//                viewHolder = MerchantItemViewHolder(v1)
//            }
//
//            ListItem.TYPE_DATE -> {
//                val v2 = inflater.inflate(R.layout.date_item, parent, false)
//                viewHolder = MerchantDetailsAdapter.DateItemViewHolder(v2)
//            }
//        }
//
//        return viewHolder
//    }
//
//    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
//        when(holder.itemViewType){
//
//            ListItem.TYPE_DATE -> {
//                val dateItem = consolidatedList.get(position) as DateItem
//                val dateItemViewHolder = holder as MerchantDetailsAdapter.DateItemViewHolder
//
//                dateItemViewHolder.itemView.findViewById<TextView>(R.id.order_date).text = dateItem.date
//
//            }
//
//            ListItem.TYPE_GENERAL -> {
//                val merchantItem = consolidatedList.get(position) as MerchantItem
//                val merchantItemViewHolder = holder as MerchantItemViewHolder
//
//                bindDataToUi(merchantItem, merchantItemViewHolder, position)
//
//            }
//        }
//    }
//
//    fun bindDataToUi(merchantItem: MerchantItem, merchantItemViewHolder: MerchantItemViewHolder, position:Int){
//        var totalLoyaltyPoints = 0.0
//
//        //merchantItem.merchantOrderDetails contains only
//        merchantItem.merchantOrderDetails?.withIndex()?.forEach { (index, merchantdetails) ->
//            merchantdetails?.forEach {
//                totalLoyaltyPoints += it.loyalty
//            }
//        }
//        var initials = merchantItem.merchantOrderDetails?.get(0)?.get(0)?.username?.substring(0,2)
//
//        merchantItemViewHolder.itemView.apply {
//            findViewById<TextView>(R.id.merch_name).text = merchantItem.merchantOrderDetails?.get(0)?.get(0)?.username
//            var LoyaltyPoints = "${totalLoyaltyPoints.toString().toNumberFormat(false)} points"
//            findViewById<TextView>(R.id.merch_loyalty_points).text = LoyaltyPoints
//            findViewById<TextView>(R.id.total_amount).text =
//                    merchantItem.merchantOrderDetails?.get(0)?.size?.minus(1)?.let {
//                        merchantItem.merchantOrderDetails?.get(0)?.get(it)?.amount.toString()
//                    }
//            findViewById<TextView>(R.id.status).text =
//                    merchantItem.merchantOrderDetails?.get(0)?.size?.minus(1)?.let {
//                        merchantItem.merchantOrderDetails?.get(0)?.get(it)?.status
//                    }
//            findViewById<TextView>(R.id.merch_initial).text = initials
//
//            if(findViewById<TextView>(R.id.status).text == "COMPLETED"){
//                findViewById<TextView>(R.id.status).setTextColor(resources.getColor(R.color.green))
//            }
//        }
//
//        merchantItemViewHolder.itemView.setOnClickListener {
//            val intent = Intent(context, MerchantDetailActivity::class.java)
//            intent.putParcelableArrayListExtra("merchantDetails", merchantItem.merchantOrderDetails?.get(0))
//            context?.startActivity(intent)
//        }
//    }

    override fun getItemCount() = merchantList.size


    fun setItems(list:ArrayList<ArrayList<TransactionEntity>>){
        merchantList.clear()
        merchantList.addAll(list)
        notifyDataSetChanged()
    }


    inner class MerchantItemViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

}