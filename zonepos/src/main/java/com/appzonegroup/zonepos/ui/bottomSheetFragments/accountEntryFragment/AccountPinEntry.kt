package com.appzonegroup.zonepos.ui.bottomSheetFragments.accountEntryFragment


import android.os.Bundle
import android.util.SparseArray
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.oakkub.android.PinEditText
import com.appzonegroup.zonepos.R
import com.appzonegroup.zonepos.db.AppDatabase
import com.appzonegroup.zonepos.db.entities.UserEntity
import com.appzonegroup.zonepos.ui.Adapters.AccountSelectionAdapter
import com.appzonegroup.zonepos.ui.bottomSheetFragments.BottomSheetFragmentRoot
import com.appzonegroup.zonepos.ui.bottomSheetFragments.QrFragment
import com.appzonegroup.zonepos.ui.utils.toNumberFormat
import com.appzonegroup.zonepos.viewmodels.SingleTransactionViewModel
import com.appzonegroup.zonepos.viewmodels.TransactionViewModelFactory
import com.appzonegroup.zonepos.viewmodels.UserViewModel
import com.appzonegroup.zonepos.viewmodels.UserViewModelFactory
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.appzonegroup.zonepos.models.Account


class AccountPinEntry : BottomSheetFragmentRoot(), View.OnClickListener {


    private val keyValues = SparseArray<String>()
    private lateinit var accountPinEntryView: View
    private lateinit var viewPager: ViewPager
    private lateinit var editText: PinEditText
    private lateinit var amountView: TextView
    var amount: String? = null
    var phoneNumber: String? = null
    var fullname: String? = null
    private lateinit var mDb: AppDatabase
    private lateinit var factory: UserViewModelFactory
    private lateinit var userViewModel: UserViewModel
    private var accountList:List<Map<String, String>>?= null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        amount = arguments?.getString("amount")
        mDb = AppDatabase.getInstance(getAppinstance())
        factory = UserViewModelFactory(mDb)
        userViewModel = ViewModelProviders.of(this, factory).get(UserViewModel::class.java)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = inflater.inflate(
        R.layout.account_pin_entry,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        accountPinEntryView = view
        viewPager = view.findViewById(R.id.viewpager)
        editText = view.findViewById(R.id.pinEditText)
        amountView = view.findViewById(R.id.amount)
        amountView.text = amount?.toNumberFormat(true)

        setUpViewPager()
        setUpPinClickListener()
    }


    private fun setUpViewPager() {
        var fragmentList = mutableListOf<Fragment>()

        userViewModel.getUserDetails().observe(this, object:Observer<List<UserEntity>> {
            override fun onChanged(userDetails: List<UserEntity>?) {
                if (userDetails?.isNotEmpty()!!) {

                    val serializedAccounts = userDetails.elementAt(0).accountListEncoded
                    phoneNumber = userDetails.elementAt(0).phonenumber
                    fullname = userDetails.elementAt(0).fullname
                    val listType = object : TypeToken<List<Map<String, String>>>() {

                    }.type

                    var accounts = Gson().fromJson<List<Map<String, String>>>(serializedAccounts, listType)

                    accountList = accounts

                    accounts.forEach { map ->
                        val account = Account(
                            accountName = map.getValue("AccountName"),
                            accountNumber = map.getValue("AccountNumber"),
                            bankName = map.getValue("BankName"),
                            accountType = map.getValue("AccountType"),
                            currencyCode = map.getValue("CurrencyCode")
                        )

                        val fragment = AccountSelectionFragment.getInstance(account)
                        fragmentList.add(fragment)
                    }

                    var accountSelectionAdapter = AccountSelectionAdapter(childFragmentManager, fragmentList)
                    viewPager.adapter = accountSelectionAdapter

                    accountPinEntryView.findViewById<ImageView>(R.id.chevron_left).setOnClickListener {
                        if (viewPager.currentItem > 0) {
                            viewPager.currentItem = viewPager.currentItem - 1
                        }
                    }

                    accountPinEntryView.findViewById<ImageView>(R.id.chevron_right).setOnClickListener {
                        if (viewPager.childCount != viewPager.currentItem) {
                            viewPager.currentItem = viewPager.currentItem + 1
                        }
                    }


                    userViewModel.getUserDetails().removeObserver(this)
                }
            }
        })




    }

    private fun setUpPinClickListener() {
        with(accountPinEntryView){
            findViewById<AppCompatTextView>(R.id.pin_one).setOnClickListener(this@AccountPinEntry)
            findViewById<AppCompatTextView>(R.id.pin_two).setOnClickListener(this@AccountPinEntry)
            findViewById<AppCompatTextView>(R.id.pin_three).setOnClickListener(this@AccountPinEntry)
            findViewById<AppCompatTextView>(R.id.pin_four).setOnClickListener(this@AccountPinEntry)
            findViewById<AppCompatTextView>(R.id.pin_five).setOnClickListener(this@AccountPinEntry)
            findViewById<AppCompatTextView>(R.id.pin_six).setOnClickListener(this@AccountPinEntry)
            findViewById<AppCompatTextView>(R.id.pin_seven).setOnClickListener(this@AccountPinEntry)
            findViewById<AppCompatTextView>(R.id.pin_eight).setOnClickListener(this@AccountPinEntry)
            findViewById<AppCompatTextView>(R.id.pin_nine).setOnClickListener(this@AccountPinEntry)
            findViewById<AppCompatTextView>(R.id.pin_zero).setOnClickListener(this@AccountPinEntry)
            findViewById<AppCompatImageView>(R.id.pin_delete).setOnClickListener(this@AccountPinEntry)
        }

        keyValues.put(R.id.pin_one, "1")
        keyValues.put(R.id.pin_two, "2")
        keyValues.put(R.id.pin_three, "3")
        keyValues.put(R.id.pin_four, "4")
        keyValues.put(R.id.pin_five, "5")
        keyValues.put(R.id.pin_six, "6")
        keyValues.put(R.id.pin_seven, "7")
        keyValues.put(R.id.pin_eight, "8")
        keyValues.put(R.id.pin_nine, "9")
        keyValues.put(R.id.pin_zero, "0")
    }

    override fun onClick(v: View?) {
       when(v?.id){
           R.id.pin_delete -> {
               deleteEntryFromEditText(v.id)
           }
           else -> {
               InputText(v?.id)
           }

       }
    }

    private fun deleteEntryFromEditText(id:Int?) {
        //todo:make the delete be one character at a time
        var length = editText.text?.length
        if(length!! > 0){
            length?.let { editText.text?.delete(length?.minus(1), it) }
        }
    }

    private fun InputText(id: Int?) {
        if(editText.text?.length == 3){
            //move to qr generator
            launchQrCode()
        } else{
            val value = id?.let { keyValues.get(it) }
            editText.text?.append(value)
        }
    }

    private fun launchQrCode() {
        //access the latest item
        var selectedAccount = accountList?.get(viewPager.currentItem)

        val bundle = Bundle()

        bundle.putString("Amount", amount)
        bundle.putString("AccountNumber", selectedAccount?.get("AccountNumber"))
        bundle.putString("AccountName", selectedAccount?.get("AccountName"))
        bundle.putString("AccountType", selectedAccount?.get("AccountType"))
        bundle.putString("CurrencyCode", selectedAccount?.get("CurrencyCode"))
        bundle.putString("BankName", selectedAccount?.get("BankName"))
        bundle.putString("PhoneNumber",  phoneNumber)
        bundle.putString("FullName",  fullname)

        val qrFragment = QrFragment()
        qrFragment.arguments = bundle
        qrFragment.show(activity!!.supportFragmentManager , qrFragment.tag)

        dismiss()
    }
}
