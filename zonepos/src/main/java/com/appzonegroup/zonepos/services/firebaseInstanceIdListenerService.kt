package com.appzonegroup.zonepos.services

import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.appzonegroup.zonepos.db.AppDatabase
import com.appzonegroup.zonepos.ui.utils.UserDetailsSharedPreferences

class MyInstanceIDListenerService:FirebaseMessagingService(){

    override fun onNewToken(p0: String?) {
        super.onNewToken(p0)

        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
            val newFcmToken = it.token
            val userDetailsSharedPreferences=
                UserDetailsSharedPreferences(applicationContext)
            Log.d("ID Service", "New fcm token -> $newFcmToken")
            userDetailsSharedPreferences.setUserToken(newFcmToken)
        }
    }

    override fun onMessageReceived(message: RemoteMessage?) {
        super.onMessageReceived(message)
        Log.d("ID Service-Status", "New message received ${message?.data}")


        var status =  message?.data?.get("status")
        var username = message?.data?.get("merchantName")
        var transactiondate = message?.data?.get("transactionRef")?.toLongOrNull()

        var mDB = AppDatabase.getInstance(this)

        status?.let { username?.let { it1 -> transactiondate?.let { it2 ->
            mDB.transactionDao().updateTransaction(it, it1, it2)
        } } }

        Log.d("ID Service-Status", status)
        Log.d("ID Service-Username", username)
        Log.d("ID Service-Date", transactiondate?.toString())
    }
}