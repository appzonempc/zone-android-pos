package com.appzonegroup.zonepos.utils

import androidx.room.TypeConverter
import java.util.*


class DateConverter{
    @TypeConverter
    fun toDate(timeStamp: Long): Date?{
        return  if(timeStamp == null) null else Date(timeStamp)
    }

    @TypeConverter
    fun toTimeStamp(date: Date): Long?{
        return date?.time
    }

}