package com.appzonegroup.zonepos.models

import android.os.Parcel
import android.os.Parcelable


data class Account(var accountName: String, var accountNumber: String, var accountType: String, var bankName:String, var currencyCode:String): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()

    ) {
        accountName= parcel.readString()
        accountNumber = parcel.readString()
        accountType = parcel.readString()
        bankName = parcel.readString()
        currencyCode =parcel.readString()
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(accountName)
        dest?.writeString(accountNumber)
        dest?.writeString(accountType)
        dest?.writeString(bankName)
        dest?.writeString(currencyCode)
    }

    override fun describeContents(): Int {
        return  0
    }

    companion object CREATOR : Parcelable.Creator<Account> {
        override fun createFromParcel(parcel: Parcel): Account {
            return Account(parcel)
        }

        override fun newArray(size: Int): Array<Account?> {
            return arrayOfNulls(size)
        }
    }
}




data class OrderDetailModel(var accountsType:String, var accountNumber:String, var oldPrice: Double, var newPrice: Double, var status: String, var date:String)

data class MerchantOrderModel(var merchantName:String, var totalLoyaltyPoint: Double, var totalAmount:Double, var status: String, var numberOfOrders: Int)

