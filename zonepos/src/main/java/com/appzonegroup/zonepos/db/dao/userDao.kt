package com.appzonegroup.zonepos.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.appzonegroup.zonepos.db.entities.UserEntity

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(userEntity: UserEntity): Long

    @Query("SELECT * FROM user LIMIT 1")
    fun loadUser(): LiveData<List<UserEntity>>
}