package com.appzonegroup.zonepos.db.entities

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "transactions")
class TransactionEntity(@PrimaryKey(autoGenerate = true) var id: Int = 0, var username:String, var status: String, var transactiondate: Long, var amount: Double, var qrdata: String, var loyalty: Double): Parcelable{
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readLong(),
        parcel.readDouble(),
        parcel.readString(),
        parcel.readDouble()
    )

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeInt(id)
        dest?.writeString(username)
        dest?.writeString(status)
        dest?.writeLong(transactiondate)
        dest?.writeDouble(amount)
        dest?.writeString(qrdata)
        dest?.writeDouble(loyalty)
    }

    override fun describeContents() = 0

    companion object CREATOR : Parcelable.Creator<TransactionEntity> {
        override fun createFromParcel(parcel: Parcel): TransactionEntity {
            return TransactionEntity(parcel)
        }

        override fun newArray(size: Int): Array<TransactionEntity?> {
            return arrayOfNulls(size)
        }
    }

}