package com.appzonegroup.zonepos.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.appzonegroup.zonepos.db.entities.TransactionEntity

@Dao
interface TransactionDao {

    @Query("SELECT * FROM transactions ORDER BY id")
    fun loadAllTransactions(): LiveData<List<TransactionEntity>>

    @Query("SELECT * FROM transactions WHERE username = :username AND status != 'pending'")
    fun loadTransactionsByUsername(username:String): LiveData<List<TransactionEntity>>

    @Query("SELECT * FROM transactions WHERE status = :type")
    fun loadTransactionType(type:String): LiveData<List<TransactionEntity>>

    @Query("SELECT * FROM transactions WHERE status != 'pending'")
    fun loadTransactionsExceptPending(): LiveData<List<TransactionEntity>>

    @Query("SELECT * FROM transactions WHERE id = :id")
    fun loadTransactionById(id:Long):LiveData<TransactionEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTransaction(transactionEntity: TransactionEntity):Long

    @Query("UPDATE transactions SET status = :status, username=:username WHERE transactiondate=:transactiondate")
    fun updateTransaction(status: String, username: String, transactiondate: Long)

    @Delete
    fun deleteTransaction(transactionEntity: TransactionEntity)


}