package com.appzonegroup.zonepos.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.appzonegroup.zonepos.db.dao.TransactionDao
import com.appzonegroup.zonepos.db.dao.UserDao
import com.appzonegroup.zonepos.db.entities.TransactionEntity
import com.appzonegroup.zonepos.db.entities.UserEntity


@Database(entities = [TransactionEntity::class, UserEntity::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase(){

    abstract fun transactionDao(): TransactionDao
    abstract fun userDao(): UserDao

    companion object {
        val DATABASE_NAME = "app_database"
        private  var SINSTANCE : AppDatabase? = null

        fun getInstance(context: Context): AppDatabase{
            return SINSTANCE ?: synchronized(this){
                SINSTANCE ?: buildDatabase(context).also { SINSTANCE = it }
            }
        }

        fun buildDatabase(context: Context): AppDatabase{
            return  Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME).build()
        }
    }
}