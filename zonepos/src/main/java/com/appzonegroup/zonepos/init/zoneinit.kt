package com.appzonegroup.zonepos.init

import android.content.Context
import android.content.Intent
import android.os.Bundle
import java.io.Serializable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.appzonegroup.zonepos.activities.ZoneActivity
import com.appzonegroup.zonepos.ui.utils.Validator.verifiyNotNull

 class Zone private constructor(private val context: Context?, private val phoneNumber:String?, private val fullname:String?, private val accountList : List<Map<String, String>>?) {

    fun show(){
        val intent = Intent(context, ZoneActivity::class.java)
        intent.putExtra("phoneNumber", phoneNumber)
        intent.putExtra("fullname", fullname)
        intent.putExtra("accountList", accountList as Serializable)
        context?.startActivity(intent)
    }


        //builder class
        class Builder{
            private var context: Context? = null
            private var phoneNumber:String? = null
            private var fullname:String? = null
            private var accountList : List<Map<String, String>>? = null

              fun addContext(ctx: Context):Builder{
                  context = verifiyNotNull(ctx, "Context is null")
                  return this
              }

              fun addPhoneNumber(number: String): Builder{
                  phoneNumber = verifiyNotNull(number, "Phone number is null")
                  return this
              }

              fun addAccountMap(list: List<Map<String, String>>): Builder{
                  verifiyNotNull(list, "Account list is null")

                  return accounts(list)
              }


              private fun accounts(accounts: List<Map<String, String>>): Builder{
                  for (account in accounts){
                      if(!account.containsKey("AccountName") && !account.containsKey("AccountNumber") && !account.containsKey("BankName") && !account.containsKey("CurrencyCode") && !account.containsKey("AccountType")){
                          throw IllegalArgumentException("Account map keys are not valid")
                      }
                      account.values.forEach {
                          if(it.isEmpty()){
                              throw IllegalArgumentException("Account map values must not be null")
                          }
                      }
                  }

                  accountList = accounts
                  return this
              }

              fun addFullName(name:String): Builder{
                  fullname = verifiyNotNull(name, "Name is null")
                  return this
              }

              fun build(): Zone{
                  if(context == null){
                      throw IllegalStateException("Context required")
                  }

                  if(phoneNumber.isNullOrEmpty()){
                      throw IllegalStateException("Phone number required")
                  }

                  if(fullname.isNullOrEmpty()){
                      throw IllegalStateException("Full name required")
                  }
                   if(accountList.isNullOrEmpty()){
                       throw IllegalStateException("Account required")
                   }

                  return Zone(context!!, phoneNumber!!, fullname!!, accountList!!)
              }
          }


}