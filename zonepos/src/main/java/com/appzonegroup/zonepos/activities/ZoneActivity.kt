package com.appzonegroup.zonepos.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import com.appzonegroup.zonepos.R
import com.appzonegroup.zonepos.db.AppDatabase
import com.appzonegroup.zonepos.db.entities.UserEntity
import com.appzonegroup.zonepos.ui.introFragments.ErrorFragment
import com.appzonegroup.zonepos.ui.introFragments.LoadingFragment
import com.appzonegroup.zonepos.ui.utils.UserDetailsSharedPreferences
import com.google.gson.Gson
import java.util.concurrent.Executors

private const val WAITING = 0
private const val SUCCESS = 1
private const val FAILURE = 2

class ZoneActivity : AppCompatActivity() {
    private val mExecutor = Executors.newFixedThreadPool(2)
    private val mDb by lazy{ AppDatabase.getInstance(this) }
    private val userDetailsSharedPreferences by lazy { UserDetailsSharedPreferences(this) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_zone)

        supportActionBar?.title = "Zone"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        when(userDetailsSharedPreferences.getInitializedStatus()){
            true -> {
                addFragment(HomeFragment(), "success")
            }
            false -> {
                addFragment(LoadingFragment(), "loading")

                //mock api call
                Handler().postDelayed({
                    retrieveDetails()
                }, 3000)
            }
        }

    }


    private fun retrieveDetails() {
        var phoneNumber = intent.getStringExtra("phoneNumber")
        var fullname = intent.getStringExtra("fullname")
        var accountList = intent.getSerializableExtra("accountList") as List<Map<String, String>>


        if (phoneNumber.isNotEmpty()  && fullname.isNotEmpty() && accountList.isNotEmpty()) {
            insertDataIntoDb(phoneNumber, fullname, Gson().toJson(accountList))

        } else {
            addFragment(ErrorFragment(), "error")
        }
    }


    private fun insertDataIntoDb(phoneNumber:String, fullname:String, serializedAccount:String) {
        mExecutor.execute {
            val userEntity = UserEntity(fullname = fullname, phonenumber = phoneNumber, accountListEncoded = serializedAccount)
            mDb.userDao().insertUser(userEntity)
        }
        userDetailsSharedPreferences.setInitializedStatus(true)
        addFragment(HomeFragment(), "success")
    }


    fun addFragment(fragment:Fragment, tag:String){
        supportFragmentManager.beginTransaction().replace(R.id.content_view, fragment, tag).commit()
    }
}
