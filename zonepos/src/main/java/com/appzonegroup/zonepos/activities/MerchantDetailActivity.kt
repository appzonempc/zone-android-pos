package com.appzonegroup.zonepos.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appzonegroup.zonepos.R
import com.appzonegroup.zonepos.db.AppDatabase
import com.appzonegroup.zonepos.db.entities.TransactionEntity
import com.appzonegroup.zonepos.ui.Adapters.MerchantDetailsAdapter
import com.appzonegroup.zonepos.ui.utils.ListArrangement
import com.appzonegroup.zonepos.ui.utils.extra
import com.appzonegroup.zonepos.ui.utils.toNumberFormat
import com.appzonegroup.zonepos.viewmodels.UserTransactionViewModel
import com.appzonegroup.zonepos.viewmodels.UserTransactionsViewModelFactory
import kotlinx.android.synthetic.main.activity_merchant_detail.*

const val KEY = "merchantDetails"

class MerchantDetailActivity : AppCompatActivity() {
    private val mDb by lazy {AppDatabase.getInstance(applicationContext)}
    private val merchantOrders by extra<ArrayList<TransactionEntity>>(KEY)
    private  var recyclerAdapter: MerchantDetailsAdapter?=null
    private  var factory: UserTransactionsViewModelFactory? =null
    private  var userTransactionViewModel: UserTransactionViewModel? =null
    val listArr =  ListArrangement()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_merchant_detail)
        requireNotNull(merchantOrders)

        supportActionBar?.title = "Back"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        initViews()
        setUpRecyclerView()


        factory = merchantOrders?.get(0)?.username?.let { UserTransactionsViewModelFactory(mDb, it) }
        userTransactionViewModel = ViewModelProviders.of(this, factory).get(UserTransactionViewModel::class.java)

        //TODO: pull based on date
        userTransactionViewModel?.getUserTransactions()?.observe(this, object:Observer<List<TransactionEntity>> {
            override fun onChanged(t: List<TransactionEntity>?) {
                //todo:transfer listArr.createConsolidatedListForSingleItem(t) to view model which handles data in the background, and just returns data to be passed to the adapter
                recyclerAdapter?.setItems(listArr.createConsolidatedListForSingleItem(t))
                userTransactionViewModel?.getUserTransactions()?.removeObserver(this)
            }

        })

    }

    private fun initViews() {
        var totalLoyaltyPoints = 0.0
        var totalAmount = 0.0

        merchantOrders?.forEach {
            totalLoyaltyPoints += it.loyalty
        }

        merchantOrders?.forEach {
            totalAmount += it.amount
        }

        merch_detail_initial.text = merchantOrders?.get(0)?.username?.substring(0,2)
        merch_detail_name.text = merchantOrders?.get(0)?.username
        merch_detail_loyalty_points.text = totalLoyaltyPoints.toString()
        merchant_total_amount.text = totalAmount.toString().toNumberFormat(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.getItemId()) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setUpRecyclerView() {
        recyclerAdapter = MerchantDetailsAdapter()
        merchant_detail_recycler_view.adapter = recyclerAdapter
        val lm = LinearLayoutManager(this)
        lm.orientation = RecyclerView.VERTICAL
        merchant_detail_recycler_view.layoutManager = lm
    }
}
