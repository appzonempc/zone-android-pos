package com.appzonegroup.zonepos.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appzonegroup.zonepos.R
import com.appzonegroup.zonepos.db.AppDatabase
import com.appzonegroup.zonepos.db.entities.TransactionEntity
import com.appzonegroup.zonepos.ui.Adapters.PendingTransactionAdapter
import com.appzonegroup.zonepos.ui.utils.ListArrangement
import com.appzonegroup.zonepos.viewmodels.TransactionTypeViewModel
import com.appzonegroup.zonepos.viewmodels.TransactionsTypeFactory
import kotlinx.android.synthetic.main.activity_transaction_detail.*

class PendingDetailActivity : AppCompatActivity() {
    private lateinit var mDb: AppDatabase
    private lateinit var pendingTransactionFactory: TransactionsTypeFactory
    private lateinit var transactionTypeViewModel: TransactionTypeViewModel
    private  var recyclerAdapter: PendingTransactionAdapter?=null
    val listArr =  ListArrangement()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction_detail)

        supportActionBar?.title = "Back"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mDb = AppDatabase.getInstance(applicationContext)
        setUpRecyclerView()



        pendingTransactionFactory = TransactionsTypeFactory(mDb, "pending")
        transactionTypeViewModel = ViewModelProviders.of(this, pendingTransactionFactory).get(TransactionTypeViewModel::class.java)

        transactionTypeViewModel.getTransactionsList().observe(this, object: Observer<List<TransactionEntity>> {
            override fun onChanged(t: List<TransactionEntity>?) {

                recyclerAdapter?.setItems(listArr.createConsolidatedListForSingleItem(t))

                transactionTypeViewModel.getTransactionsList().removeObserver(this)
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.getItemId()) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setUpRecyclerView() {
        recyclerAdapter = PendingTransactionAdapter()
        detail_recyclerView.adapter = recyclerAdapter
        val lm = LinearLayoutManager(this)
        lm.orientation = RecyclerView.VERTICAL
        detail_recyclerView.layoutManager = lm


    }
}
