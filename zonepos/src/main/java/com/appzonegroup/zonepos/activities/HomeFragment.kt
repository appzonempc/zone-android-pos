package com.appzonegroup.zonepos.activities


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.ViewPager

import com.appzonegroup.zonepos.R
import com.appzonegroup.zonepos.ui.Adapters.HomePageAdapter
import com.appzonegroup.zonepos.ui.HistoryFragment
import com.appzonegroup.zonepos.ui.PaymentFragment
import com.appzonegroup.zonepos.ui.bottomSheetFragments.*
import com.appzonegroup.zonepos.ui.utils.UserDetailsSharedPreferences
import com.google.android.material.tabs.TabLayout
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolbar()
        initializeViews(view)
        getUserToken()
    }

    private fun getUserToken() {

        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
            var  fcmToken = it.token
            val userDetailsSharedPreferences=
                activity?.applicationContext?.let { it1 -> UserDetailsSharedPreferences(it1) }
            Log.d(TAG, fcmToken)
            userDetailsSharedPreferences?.setUserToken(fcmToken)
        }

    }

    private fun initToolbar(){
//        activity.?.title = "Back"
//        supportActionBar?.setHomeButtonEnabled(true)
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }

    private fun initializeViews(view: View) {
        val fragmentList = listOf(HistoryFragment(), PaymentFragment())
        val fragmentTitleList = listOf("History", "Payment")

        val activity = activity

        val viewPagerAdapter = HomePageAdapter(activity, fragmentList, fragmentTitleList, activity!!.supportFragmentManager)
        view.findViewById<ViewPager>(R.id.view_pager).adapter = viewPagerAdapter
        view.findViewById<TabLayout>(R.id.tablayout).setupWithViewPager(view_pager)

        view.findViewById<TabLayout>(R.id.tablayout).addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
            override fun onTabReselected(p0: TabLayout.Tab?) {

            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                if(tab?.position == 1){
                    val amountEntryFragment = AmountEntryFragment()
                    amountEntryFragment.show(activity.supportFragmentManager, amountEntryFragment.tag)
                }
            }
        })
    }





    companion object {
        private val TAG  = HomeFragment::class.java.simpleName
    }
}
