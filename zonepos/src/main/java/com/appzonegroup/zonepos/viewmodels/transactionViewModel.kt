package com.appzonegroup.zonepos.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.appzonegroup.zonepos.db.AppDatabase

class TransactionViewModelFactory(var mDb:AppDatabase, var id: Long): ViewModelProvider.NewInstanceFactory(){

    override fun <T : ViewModel?> create(modelClass: Class<T>): T = SingleTransactionViewModel(mDb, id) as T
}

class SingleTransactionViewModel(var mDb:AppDatabase, var id:Long):ViewModel(){

    fun getUpdateTransaction() = mDb.transactionDao().loadTransactionById(id)

}