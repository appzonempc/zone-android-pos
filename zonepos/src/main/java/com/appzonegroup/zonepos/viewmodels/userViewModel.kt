package com.appzonegroup.zonepos.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.appzonegroup.zonepos.db.AppDatabase

class UserViewModelFactory(val mdb:AppDatabase): ViewModelProvider.NewInstanceFactory(){
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = UserViewModel(mdb) as T
}

class UserViewModel(val mdb:AppDatabase): ViewModel() {

    fun getUserDetails() = mdb.userDao().loadUser()
}