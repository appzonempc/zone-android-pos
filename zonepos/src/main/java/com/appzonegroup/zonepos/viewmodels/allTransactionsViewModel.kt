package com.appzonegroup.zonepos.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.appzonegroup.zonepos.db.AppDatabase
import com.appzonegroup.zonepos.db.entities.TransactionEntity


class TransactionViewModel(application: Application) : AndroidViewModel(application){

    val transactions: LiveData<List<TransactionEntity>>

    init {
        val appDatabase = AppDatabase.getInstance(this.getApplication())
        transactions = appDatabase.transactionDao().loadAllTransactions()
    }

    fun getAllTransactions() = transactions
}