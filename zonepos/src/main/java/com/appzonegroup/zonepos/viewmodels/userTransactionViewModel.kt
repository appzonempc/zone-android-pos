package com.appzonegroup.zonepos.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.appzonegroup.zonepos.db.AppDatabase

class UserTransactionsViewModelFactory(val mDb: AppDatabase, val username: String) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T  = UserTransactionViewModel(mDb, username) as T
}

class UserTransactionViewModel(val mDb: AppDatabase, val username: String) : ViewModel() {

    fun getUserTransactions() = mDb.transactionDao().loadTransactionsByUsername(username)

}