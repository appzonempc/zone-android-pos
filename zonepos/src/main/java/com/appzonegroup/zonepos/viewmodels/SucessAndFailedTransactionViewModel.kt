package com.appzonegroup.zonepos.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.appzonegroup.zonepos.db.AppDatabase

class SuccessAndFailedTransactionsFactory(var mDb:AppDatabase): ViewModelProvider.NewInstanceFactory(){

    override fun <T : ViewModel?> create(modelClass: Class<T>): T = SuccessAndFailedTransactionViewModel(mDb) as T
}

class SuccessAndFailedTransactionViewModel(var mDb: AppDatabase): ViewModel(){

    fun getSuccessAndFailedTransaction() = mDb.transactionDao().loadTransactionsExceptPending()
}