package com.appzonegroup.zonepos.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.appzonegroup.zonepos.db.AppDatabase


class TransactionsTypeFactory(val mDb: AppDatabase, val status: String): ViewModelProvider.NewInstanceFactory(){

    override fun <T : ViewModel?> create(modelClass: Class<T>): T  = TransactionTypeViewModel(mDb, status) as T
}

class TransactionTypeViewModel(val mDb: AppDatabase, val status: String) : ViewModel(){

    fun getTransactionsList() = mDb.transactionDao().loadTransactionType(status)
}