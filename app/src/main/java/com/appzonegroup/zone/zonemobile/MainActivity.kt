package com.appzonegroup.zone.zonemobile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.appzonegroup.zonepos.init.Zone
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)




        floatingActionButton.setOnClickListener {

            launchZone()

        }
    }

    private fun launchZone() {
        val map= HashMap<String, String>().apply {
            put("AccountName", "Test account")
            put("AccountNumber", "2032224739")
            put("AccountType", "Savings")
            put("CurrencyCode", "NGN")
            put("BankName", "UBA")
        }

        val list = ArrayList<HashMap<String,String>>()
        list.add(map)
        list.add(map)
        list.add(map)



        var zone = Zone.Builder().addContext(this)
            .addFullName("richard ekene")
            .addPhoneNumber("234814738855")
            .addAccountMap(list)
            .build()

        zone.show()

    }
}
